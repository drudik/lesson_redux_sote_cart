import style from './style.module.scss'
import { Link } from 'react-router-dom';
import { FaCartArrowDown } from 'react-icons/fa';
import { useSelector } from 'react-redux'

export default function Header() {
    const counter = useSelector(state => state.cartReducer.counter);
    return(
        <div className={style.header}>
            <div>
                <Link className={style.header_link} to="/" >home</Link>
                <Link className={style.header_link} to="/shop" >shop</Link>
            </div>
            <Link className={style.header_link_counter} to="/checkout" >
                <span className={style.header_link_item}>{counter > 8 ? ('9+') : (counter)}</span>
                <div> <FaCartArrowDown className={style.cartBtn} /></div>
            </Link>
            
        </div>
    );
}