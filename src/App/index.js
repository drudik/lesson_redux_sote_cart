import './style.module.scss';
import { Routes, Route} from 'react-router-dom';

import Header from '../components/Header'
import Home from '../pages/Main';
import Shop from '../pages/Shop';
import Checkout from '../pages/Cart';


function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Home/> } />
        <Route path="/shop" element={<Shop/> } />
        <Route path="/checkout" element={<Checkout/>} />
      </Routes>
    </div>
  );
}

export default App;
