import update from 'immutability-helper';

export const addToCart = (state, action) => {
    return update( state, {products_items: {$push: [action.item]}, counter: {$set: state.counter + 1} } );
}

export const increment = (state, action) => {
    return update(state, {products_item: {$set: action.item}, counter: {$set: state.counter + 1 }});
}

export const decrement = (state, action) => {
    return update(state, {products_item: {$set: action.item}, counter: {$set: state.counter - 1 }});
}

export const remove = (state, action) => {
    const index = state.products_items.findIndex(item => item.id === action.id);
    const itemCounter = state.products_items.find(el => el.id === action.id).counter;
    return update(state, {products_items: {$splice: [[index, 1]]}, counter: {$set: state.counter - itemCounter}});
}