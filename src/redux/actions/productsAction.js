import update from 'immutability-helper';

export const getProducts = (state, action) => {
    return update(state, {products: {$set: action.result}, status: {$set: 'ok'} } );
}

export const statusLoading = (state, action) => {
    return update(state, {status: {$set: action.status} } )
}
