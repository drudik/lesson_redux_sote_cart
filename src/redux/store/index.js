import {createStore, combineReducers} from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web


import {cartReducer} from '../reducers/carReducer';
import {productReducer} from '../reducers/productReducer';


const rootReducer = combineReducers({productReducer, cartReducer});

const persistConfig = {
    key: 'root',
    storage,
  }

  const persistedReducer = persistReducer(persistConfig, rootReducer)

// export const store = createStore(rootReducer);

let store = createStore(persistedReducer)
let persistor = persistStore(store)

export { store, persistor}
