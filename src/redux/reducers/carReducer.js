import { 
    ADD_TO_CART, 
    INCREMENT_ITEM_IN_CART, 
    DECREMENT_ITEM_IN_CART,
    REMOVE_FROM_CART 
} from "../types"
import { addToCart, increment, decrement, remove } from '../actions/cartAction'

const defaultState = {
    products_items: [],
    counter: 0,
    total_price: 0 
}


export const cartReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ADD_TO_CART: return addToCart(state, action);
        case INCREMENT_ITEM_IN_CART: return increment(state, action);
        case DECREMENT_ITEM_IN_CART: return decrement(state, action);
        case REMOVE_FROM_CART: return remove(state, action);
        default: return state;
    }
}



