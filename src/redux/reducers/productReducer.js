import { LOADING_PRODUCTS, GET_PRODUCTS } from "../types"
import { getProducts, statusLoading } from '../actions/productsAction'

const defaultState = {
    products: [],
    status: null
}

export const productReducer = (state = defaultState, action) => {
    switch (action.type) {
        case LOADING_PRODUCTS: return statusLoading(state, action);
        case GET_PRODUCTS: return getProducts(state, action);
        default: return state;
    }
}
