import style from './style.module.scss';
import {Link} from 'react-router-dom';

export default function Empty() {
    return (
        <>
            <div className={style.message_container}>
                <div>
                    <h2>cart is empty</h2>
                </div>
                <Link to="/shop">return to shop</Link>
            </div>
        </>
    );
}