import style from './style.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { AiFillDelete, AiFillMinusCircle, AiFillPlusCircle } from 'react-icons/ai';

export default function Checkout(props) {
    const { products_items } = useSelector(store => store.cartReducer); 
    const dispatch = useDispatch(); 
    
    const increment = (id) => {
        const item = products_items.find(el => el.id === id);
        item.counter++
        dispatch({type: 'INCREMENT_ITEM_IN_CART', item}); 
    }

    const decrement = (id) => {
        const item = products_items.find(el => el.id === id);
        item.counter--
        dispatch({type: 'DECREMENT_ITEM_IN_CART', item}); 
    }

    const remove = (id) => {
        dispatch({type: 'REMOVE_FROM_CART', id});
    }

    return (
        <div className={style.main_container}>
            {products_items.map(item => (
                    <div key={item.id} className={style.cart_container}>
                        <div className={style.item_block}>
                            <div>
                                <img src={item.image} alt={item.title}/>
                                <h4>{item.title}</h4>
                            </div>
                            <p>{item.price}$</p>
                        </div>
                        <div className={style.counter_block}>
                            <AiFillMinusCircle onClick={ () => ( decrement(item.id) ) }/>
                                <p>{item.counter}</p>
                            <AiFillPlusCircle onClick={ () => ( increment(item.id) ) }/>
                        </div>
                        <div>
                            <AiFillDelete className={style.delete_icon} onClick={ () => ( remove(item.id) )}/>
                        </div>
                    </div>  
                )) 
            }
                
        </div>    
    );
}