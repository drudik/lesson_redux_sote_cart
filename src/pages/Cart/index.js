import Empty from './Empty';
import Checkout from './Checkout'
import { useSelector } from 'react-redux'


function Cart() {
    const cart = useSelector(state => state.cartReducer.products_items);   
    return (
        <>
            { cart.length === 0 ? (<Empty />) : (<Checkout props={cart} />) }
        </>
    );
}

export default Cart;


   


