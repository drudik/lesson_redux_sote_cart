import style from './style.module.scss';
import {useDispatch, useSelector} from 'react-redux';
import { useEffect } from 'react';

import Loader from './Loader';

export default function Shop() {
    //products api
    const { products, status } = useSelector(store => store.productReducer);
    const { products_items } = useSelector(store => store.cartReducer); 

    const dispatch = useDispatch();
    
    const fetchProducts = async () => {  
        const response = await fetch('https://fakestoreapi.com/products');
        const result = await response.json();
        dispatch({type: 'LOADING_PRODUCTS', status: 'loading'})
        dispatch({type: 'GET_PRODUCTS', result});
    }

    const getId = (id) => {
        const checkId = products_items.find(el => el.id === id);
        if (!checkId) {
            const item = products.find(el => el.id === id);
            item.counter = 1;           
            return dispatch({type: 'ADD_TO_CART', item});
        }
        if (checkId) {
                const item = products_items.find(el => el.id === id);
                item.counter++
                return dispatch({type: 'INCREMENT_ITEM_IN_CART', item}); 
            }
        }
        
    useEffect (() => {
        if (status === null) {
            fetchProducts();
        }    
    }, []);

    return(
        <>
        {status === null ? <Loader /> : 
            (<div className={style.shop_container}>
                {products.map(item => (
                    <div key={item.id} className={style.product_card}>
                        <img src={item.image} alt={item.title}/>
                        <h4>{item.title}</h4>
                        <h6>{item.price + ' ' + '$'}</h6> 
                        <button type="button" onClick={ () => ( getId(item.id) ) }>add to cart</button>
                    </div>
                ))}
            </div>) 
        }
        </> 
    );
}