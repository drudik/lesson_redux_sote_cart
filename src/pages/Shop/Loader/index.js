import { Circles, RevolvingDot } from 'react-loader-spinner'
import style from './style.module.scss'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
export default function Loader() {
    return (
        <div className={style.loader_container}>
            <Circles type="Circles" color="#00BFFF" height={40} width={40}/>
        </div>
    );
}